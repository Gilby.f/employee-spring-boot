package com.example.bookstore.modelDTO;

public class GradeDTO {

	private Long id;
	private Double baseProduction;
	private String quality;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getBaseProduction() {
        return baseProduction;
    }

    public void setBaseProduction(double d) {
        this.baseProduction = d;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }
}
