package com.example.bookstore.controller;

import com.example.bookstore.model.Publisher;
import com.example.bookstore.modelDTO.PublisherDTO;
import com.example.bookstore.repository.PublisherRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/publisher")
public class PublisherController {

    @Autowired
    PublisherRepository publisherRepository;
    
    ModelMapper modelMapper = new ModelMapper();

    // Get All Notes
    @GetMapping("")
    public Map<String,Object> getAllPublisher() {
        List<Publisher> listPublisherEntity = publisherRepository.findAll();
        List<PublisherDTO> listPublisherDTO = new ArrayList<PublisherDTO>();
        Map<String, Object> result = new HashMap<String, Object>();

        for (Publisher publisherEntity : listPublisherEntity) {
            PublisherDTO publisherDTO = new PublisherDTO();

            publisherDTO = modelMapper.map(publisherEntity, PublisherDTO.class);

            listPublisherDTO.add(publisherDTO);
        }
        result.put("status", 200);
        result.put("Messages", "Read all data is success");
        result.put("Data", listPublisherDTO);

        return result;
    }

    // Get a Single Note
    @GetMapping("/{id}")
    public Map<String, Object> getPublisherById(@PathVariable(value = "id") final Long publisherId) {

        Map<String, Object> result = new HashMap<String, Object>();
        Publisher publisherEntity = publisherRepository.findById(publisherId).get();
        PublisherDTO publisherDTO = new PublisherDTO();

        publisherDTO = modelMapper.map(publisherEntity, PublisherDTO.class);

        result.put("status", 200);
        result.put("Messages", "Read by ID data is success");
        result.put("Data", publisherDTO);

        return result;

    }

    // Create a new Note
    @PostMapping("/create")
    public Map<String,Object> createPublisher(@Valid @RequestBody  PublisherDTO publisherDTO) {
        Map<String,Object> result = new HashMap<String,Object>();
        Publisher publisherEntity = new Publisher();

        publisherEntity = modelMapper.map(publisherDTO, Publisher.class );
        publisherRepository.save(publisherEntity);

        result.put("Status", 200);
        result.put("Messages", "Create Data Success");
        result.put("Data", publisherDTO);
        
        return result;
    }


    // Update a Note
    @PutMapping("/update/{id}")
    public Map<String,Object> updatePublisher(@PathVariable(value = "id")  Long publisherId,
        @Valid @RequestBody  PublisherDTO publisherDTO) {
    
        Map<String,Object> result = new HashMap<String,Object>();
        
        Publisher publisherEntity = modelMapper.map(publisherDTO, Publisher.class);
        
        publisherEntity.setId(publisherId);
        publisherRepository.save(publisherEntity);
                
        result.put("Status", 200);
        result.put("Messages", "Update Data Success");
        result.put("Data", publisherDTO);

        return result;
    }
    // Delete a Note
    @DeleteMapping("/delete/{id}")
    public Map<String,Object> deletePublisher(@PathVariable(value = "id") final Long publisherId) {
        
        Publisher publisherEntity = publisherRepository.findById(publisherId).get();
        Map<String,Object> result = new HashMap<String,Object>();
    
        publisherRepository.delete(publisherEntity);
        
        result.put("Status", 200);
        result.put("Messages", "Delete Data Success");
    
        return result;
    }
}