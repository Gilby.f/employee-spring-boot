package com.example.bookstore.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.bookstore.model.Books;
import com.example.bookstore.repository.BookRepository;

@RestController
@RequestMapping("/api/price")
public class CalculationBasePriceController {

	@Autowired
	BookRepository bookRepository;
	
	//Calculation Price Books
	@PutMapping("/update_price")
	public HashMap<String, Object> updatePrice(){
	
		final List<Books> bookPrice = bookRepository.findAll();
			
		for (final Books book : bookPrice) {	
			double basicPrice, baseProduction, ratePrice;
			final Date releaseDate = book.getRealeaseDate();
			final int releaseYear = releaseDate.getYear();
			final int currentYear = Calendar.getInstance().get(Calendar.YEAR);
			
			final String bookReleaseCountry = book.getPublisher().getCountry(), ID = "Indonesia";
			double finalPrice, taxRate;
			
			if (releaseYear == currentYear) {
				ratePrice = 1.5;
			} else {
				ratePrice = 1.3;
			}
			
			baseProduction = book.getPublisher().getGrade().getBaseProduction();
			basicPrice = ratePrice * baseProduction;
			
			if (bookReleaseCountry.equalsIgnoreCase(ID)) {
				taxRate = 0.05;
			} else {
				taxRate = 0.1;
			}
			
			finalPrice = basicPrice + (basicPrice * taxRate);
			book.setPrice(finalPrice);
			
			bookRepository.save(book);
		}
		
		final HashMap<String, Object> messageCalculation = new HashMap<String, Object>();
		messageCalculation.put("Status", 200);
		messageCalculation.put("Message", "Book Price Calculation Is Success");
		
		return messageCalculation;
	}
	
}
