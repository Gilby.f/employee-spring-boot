package com.example.bookstore.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.example.bookstore.exception.ResourceNotFoundException;
import com.example.bookstore.model.Author;
import com.example.bookstore.modelDTO.AuthorDTO;
import com.example.bookstore.repository.AuthorRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/author")
public class AuthorController{

    @Autowired
    AuthorRepository authorRepository;

    //Get All data Author
    @GetMapping("")
    public Map<String, Object> getReadAllAuthor() {
        List<Author> listAuthorEntity = authorRepository.findAll();
        List<AuthorDTO> listAuthorDTO = new ArrayList<AuthorDTO>();
        Map<String, Object> result = new HashMap<String, Object>();

        for (Author authorEntity : listAuthorEntity) {
            AuthorDTO authorDTO = new AuthorDTO();

            authorDTO.setId(authorEntity.getId());
            authorDTO.setFirstName(authorEntity.getFirstName());
            authorDTO.setLastName(authorEntity.getLastName());

            listAuthorDTO.add(authorDTO);
        }
        result.put("status", 200);
        result.put("Messages", "Read all data is success");
        result.put("Data", listAuthorDTO);

        return result;
    }

    //Get a Single data Author
    @GetMapping("/{id}")
    public Map<String, Object> getAuthorById(@PathVariable(value = "id") final Long authorId){
        
        Map<String, Object> result = new HashMap<String,Object>();
        Author authorEntity = authorRepository.findById(authorId)
        .orElseThrow(() -> new ResourceNotFoundException("Author", "id", authorId));
        AuthorDTO authorDTO = new AuthorDTO();

        authorDTO.setId(authorEntity.getId());
        authorDTO.setFirstName(authorEntity.getFirstName());
        authorDTO.setLastName(authorEntity.getLastName());

        result.put("status", 200);
        result.put("Messages", "Read by ID data is success");
        result.put("Data", authorDTO);

        return result;
    }
    
    //Create new data Author
    @PostMapping("/create")
    public Map<String, Object> createAuthor(@Valid @RequestBody final AuthorDTO authorDTO){
        
        Map<String, Object> result = new HashMap<String, Object>();
        Author authorEntity = new Author();

        authorEntity.setId(authorDTO.getId());
        authorEntity.setFirstName(authorDTO.getFirstName());
        authorEntity.setLastName(authorDTO.getLastName());
        authorRepository.save(authorEntity);

        result.put("Status", 200);
        result.put("Messages", "Create Data Success");
        result.put("Data", authorDTO);

        return result;
    }
    
    // Update a Note
    @PutMapping("/update/{id}")
    public Map<String, Object> updateAuthor(@PathVariable(value = "id") Long authorId,
        @Valid @RequestBody final AuthorDTO authorDTO) {
    
        Map<String, Object> result = new HashMap<String,Object>();
        Author authorEntity = authorRepository.findById(authorId).get();

        authorEntity.setFirstName(authorDTO.getFirstName());
        authorEntity.setLastName(authorDTO.getLastName());
        
        authorRepository.save(authorEntity);
        
        result.put("Status", 200);
        result.put("Messages", "Update Data Success");
        result.put("Data", authorDTO);

        return result;
    }

    
    // Delete a Note
    @DeleteMapping("/delete/{id}")
    public Map<String, Object> deleteAuthor(@PathVariable(value = "id") Long authorId) {
        
        Author authorEntity = authorRepository.findById(authorId).get();
        Map<String,Object> result = new HashMap<String, Object>();
    
        authorRepository.delete(authorEntity);
    
        result.put("Status", 200);
        result.put("Messages", "Delete Data Success");

        return result;
    }

}