package com.example.bookstore.controller;

import com.example.bookstore.exception.ResourceNotFoundException;
import com.example.bookstore.model.Books;
import com.example.bookstore.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/books")
public class BookController {

    @Autowired
    BookRepository bookRepository;

    // Get All Notes
    @GetMapping("")
    public List<Books> getAllBooks() {
        return bookRepository.findAll();
    }
    // Create a new Note
    @PostMapping("/create")
    public Books createBook(@Valid @RequestBody final Books book) {
        return bookRepository.save(book);
    }
    // Get a Single Note
    @GetMapping("/{id}")
    public Books getBookById(@PathVariable(value = "id") final Long bookId) {
        return bookRepository.findById(bookId)
                .orElseThrow(() -> new ResourceNotFoundException("Book", "id", bookId));
    }
    // Update a Note
    @PutMapping("/update/{id}")
    public Books updateBook(@PathVariable(value = "id") final Long bookId,
                                            @Valid @RequestBody final Books bookDetails) {
    
        final Books book = bookRepository.findById(bookId)
                .orElseThrow(() -> new ResourceNotFoundException("Book", "id", bookId));
    
        book.setTitle(bookDetails.getTitle());
        book.setDescription(bookDetails.getDescription());
        book.setRealeaseDate(bookDetails.getRealeaseDate());
        book.setAuthor(bookDetails.getAuthor());
        book.setCategory(bookDetails.getCategory());
        book.setPublisher(bookDetails.getPublisher());
        
        final Books updatedBook = bookRepository.save(book);
        return updatedBook;
    }
    // Delete a Note
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteBook(@PathVariable(value = "id") final Long bookId) {
        final Books book = bookRepository.findById(bookId)
                .orElseThrow(() -> new ResourceNotFoundException("Book", "id", bookId));
    
        bookRepository.delete(book);
    
        return ResponseEntity.ok().build();
    }
}