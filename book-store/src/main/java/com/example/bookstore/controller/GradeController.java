package com.example.bookstore.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.example.bookstore.model.Grade;
import com.example.bookstore.modelDTO.GradeDTO;
import com.example.bookstore.repository.GradeRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/grade")
public class GradeController {

    @Autowired
    GradeRepository gradeRepository;

    // Get All Data Grade
    @GetMapping("")
    public Map<String, Object> getAllGrade() {
        List<Grade> listGradeEntity = gradeRepository.findAll();
        List<GradeDTO> listGradeDTO = new ArrayList<GradeDTO>();
        Map<String, Object> result = new HashMap<String, Object>();

        for (Grade gradeEntity : listGradeEntity) {
            GradeDTO gradeDTO =  new GradeDTO();
            
            gradeDTO.setId(gradeEntity.getId());
            gradeDTO.setQuality(gradeEntity.getQuality());
            gradeDTO.setBaseProduction(gradeEntity.getBaseProduction());

            listGradeDTO.add(gradeDTO);
        }
        result.put("status", 200);
        result.put("Messages", "Read all data is success");
        result.put("Data", listGradeDTO);

        return result;
        
    }

    
    // Get a Single Note
    @GetMapping("/{id}")
    public Map<String,Object> getGradeById(@PathVariable(value = "id")  Long gradeId) {
        
         Map<String, Object> result = new HashMap<String, Object>();
         Grade gradeEntity = gradeRepository.findById(gradeId).get();
         GradeDTO gradeDTO = new GradeDTO();

        gradeDTO.setId(gradeEntity.getId());
        gradeDTO.setQuality(gradeEntity.getQuality());
        gradeDTO.setBaseProduction(gradeEntity.getBaseProduction());

        result.put("status", 200);
        result.put("Messages", "Read by ID data is success");
        result.put("Data", gradeDTO);

        return result;

    }

    // Create a new Note
    @PostMapping("/create")
    public Map<String,Object> createGrade(@Valid @RequestBody  GradeDTO gradeDTO) {
         Map<String,Object> result = new HashMap<String,Object>();
         Grade gradeEntity = new Grade();

        gradeEntity.setId(gradeDTO.getId());
        gradeEntity.setQuality(gradeDTO.getQuality());
        gradeEntity.setBaseProduction(gradeDTO.getBaseProduction());
        gradeRepository.save(gradeEntity);
        
        result.put("Status", 200);
        result.put("Messages", "Create Data Success");
        result.put("Data", gradeDTO);
        
        return result;
    }

    // Update a Note
    @PutMapping("/update/{id}")
    public Map<String,Object> updateGrade(@PathVariable(value = "id")  Long gradeId,
        @Valid @RequestBody  GradeDTO gradeDTO) {
    
         Map<String,Object> result = new HashMap<String,Object>();
         Grade gradeEntity = gradeRepository.findById(gradeId).get();

        gradeEntity.setQuality(gradeDTO.getQuality());
        gradeEntity.setBaseProduction(gradeDTO.getBaseProduction());
        
        gradeRepository.save(gradeEntity);
                
        result.put("Status", 200);
        result.put("Messages", "Update Data Success");
        result.put("Data", gradeDTO);

        return result;
    }
    // Delete a Note
    @DeleteMapping("/delete/{id}")
    public Map<String,Object> deleteCategory(@PathVariable(value = "id")  Long gradeId) {
        
         Grade gradeEntity = gradeRepository.findById(gradeId).get();
         Map<String,Object> result = new HashMap<String,Object>();

        gradeRepository.delete(gradeEntity);
    
        result.put("Status", 200);
        result.put("Messages", "Delete Data Success");

        return result;
    }
}