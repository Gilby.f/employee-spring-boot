package com.example.employee.employee.modelDTO;

public class ListKemampuanDTO {

	private int idListKemampuan;
	private KaryawanDTO karyawanDTO;
	private KemampuanDTO kemampuanDTO;
	private Integer nilaiKemampuan;
	public int getIdListKemampuan() {
		return idListKemampuan;
	}
	public void setIdListKemampuan(int idListKemampuan) {
		this.idListKemampuan = idListKemampuan;
	}
	public KaryawanDTO getKaryawanDTO() {
		return karyawanDTO;
	}
	public void setKaryawanDTO(KaryawanDTO karyawanDTO) {
		this.karyawanDTO = karyawanDTO;
	}
	public KemampuanDTO getKemampuanDTO() {
		return kemampuanDTO;
	}
	public void setKemampuanDTO(KemampuanDTO kemampuanDTO) {
		this.kemampuanDTO = kemampuanDTO;
	}
	public Integer getNilaiKemampuan() {
		return nilaiKemampuan;
	}
	public void setNilaiKemampuan(Integer nilaiKemampuan) {
		this.nilaiKemampuan = nilaiKemampuan;
	}
}