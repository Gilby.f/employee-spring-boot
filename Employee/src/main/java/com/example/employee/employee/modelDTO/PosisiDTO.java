package com.example.employee.employee.modelDTO;

public class PosisiDTO {

	private int idPosisi;
	private String namaPosisi;

	public int getIdPosisi() {
		return idPosisi;
	}

	public void setIdPosisi(int idPosisi) {
		this.idPosisi = idPosisi;
	}

	public String getNamaPosisi() {
		return namaPosisi;
	}

	public void setNamaPosisi(String namaPosisi) {
		this.namaPosisi = namaPosisi;
	}
}

