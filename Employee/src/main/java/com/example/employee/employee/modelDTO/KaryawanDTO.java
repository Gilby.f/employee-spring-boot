package com.example.employee.employee.modelDTO;

import java.sql.Date;

public class KaryawanDTO {

	private int idKaryawan;
	private AgamaDTO agamaDTO;
	private PenempatanDTO penempatanDTO;
	private PosisiDTO posisiDTO;
	private TingkatanDTO tingkatanDTO;
	private String nama;
	private String noKtp;
	private String alamat;
	private Date tanggalLahir;
	private Integer masaKerja;
	private Short statusPernikahan;
	private Date kontrakAwal;
	private Date kontrakAkhir;
	private String jenisKelamin;
	private Integer jumlahAnak;
	
	public int getIdKaryawan() {
		return idKaryawan;
	}
	public void setIdKaryawan(int idKaryawan) {
		this.idKaryawan = idKaryawan;
	}
	public AgamaDTO getAgamaDTO() {
		return agamaDTO;
	}
	public void setAgamaDTO(AgamaDTO agamaDTO) {
		this.agamaDTO = agamaDTO;
	}
	public PenempatanDTO getPenempatanDTO() {
		return penempatanDTO;
	}
	public void setPenempatanDTO(PenempatanDTO penempatanDTO) {
		this.penempatanDTO = penempatanDTO;
	}
	public PosisiDTO getPosisiDTO() {
		return posisiDTO;
	}
	public void setPosisiDTO(PosisiDTO posisiDTO) {
		this.posisiDTO = posisiDTO;
	}
	public TingkatanDTO getTingkatanDTO() {
		return tingkatanDTO;
	}
	public void setTingkatanDTO(TingkatanDTO tingkatanDTO) {
		this.tingkatanDTO = tingkatanDTO;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getNoKtp() {
		return noKtp;
	}
	public void setNoKtp(String noKtp) {
		this.noKtp = noKtp;
	}
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	public Date getTanggalLahir() {
		return tanggalLahir;
	}
	public void setTanggalLahir(Date tanggalLahir) {
		this.tanggalLahir = tanggalLahir;
	}
	public Integer getMasaKerja() {
		return masaKerja;
	}
	public void setMasaKerja(Integer masaKerja) {
		this.masaKerja = masaKerja;
	}
	public Short getStatusPernikahan() {
		return statusPernikahan;
	}
	public void setStatusPernikahan(Short statusPernikahan) {
		this.statusPernikahan = statusPernikahan;
	}
	public Date getKontrakAwal() {
		return kontrakAwal;
	}
	public void setKontrakAwal(Date kontrakAwal) {
		this.kontrakAwal = kontrakAwal;
	}
	public Date getKontrakAkhir() {
		return kontrakAkhir;
	}
	public void setKontrakAkhir(Date kontrakAkhir) {
		this.kontrakAkhir = kontrakAkhir;
	}
	public String getJenisKelamin() {
		return jenisKelamin;
	}
	public void setJenisKelamin(String jenisKelamin) {
		this.jenisKelamin = jenisKelamin;
	}
	public Integer getJumlahAnak() {
		return jumlahAnak;
	}
	public void setJumlahAnak(Integer jumlahAnak) {
		this.jumlahAnak = jumlahAnak;
	}
}