package com.example.employee.employee.modelDTO;

import java.math.BigDecimal;

public class TunjanganPegawaiDTO {

	private int idTunjanganPegawai;
	private PosisiDTO posisiDTO;
	private TingkatanDTO tingkatanDTO;
	private BigDecimal besaranTujnaganPegawai;
	public int getIdTunjanganPegawai() {
		return idTunjanganPegawai;
	}
	public void setIdTunjanganPegawai(int idTunjanganPegawai) {
		this.idTunjanganPegawai = idTunjanganPegawai;
	}
	public PosisiDTO getPosisiDTO() {
		return posisiDTO;
	}
	public void setPosisiDTO(PosisiDTO posisiDTO) {
		this.posisiDTO = posisiDTO;
	}
	public TingkatanDTO getTingkatanDTO() {
		return tingkatanDTO;
	}
	public void setTingkatanDTO(TingkatanDTO tingkatanDTO) {
		this.tingkatanDTO = tingkatanDTO;
	}
	public BigDecimal getBesaranTujnaganPegawai() {
		return besaranTujnaganPegawai;
	}
	public void setBesaranTujnaganPegawai(BigDecimal besaranTujnaganPegawai) {
		this.besaranTujnaganPegawai = besaranTujnaganPegawai;
	}
}