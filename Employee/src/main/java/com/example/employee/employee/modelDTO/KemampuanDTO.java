package com.example.employee.employee.modelDTO;

public class KemampuanDTO {

	private int idKemampuan;
	private KategoriKemampuanDTO KategoriKemampuanDTO ;
	private String namaKemampuan;
	public int getIdKemampuan() {
		return idKemampuan;
	}
	public void setIdKemampuan(int idKemampuan) {
		this.idKemampuan = idKemampuan;
	}
	public KategoriKemampuanDTO getKategoriKemampuanDTO() {
		return KategoriKemampuanDTO;
	}
	public void setKategoriKemampuanDTO(KategoriKemampuanDTO kategoriKemampuanDTO) {
		KategoriKemampuanDTO = kategoriKemampuanDTO;
	}
	public String getNamaKemampuan() {
		return namaKemampuan;
	}
	public void setNamaKemampuan(String namaKemampuan) {
		this.namaKemampuan = namaKemampuan;
	}
}