package com.example.employee.employee.modelDTO;

import java.math.BigDecimal;

public class PenempatanDTO {

	private int idPenempatan;
	private String kotaPenempatan;
	private BigDecimal umkPenempatan;

	public int getIdPenempatan() {
		return idPenempatan;
	}

	public void setIdPenempatan(int idPenempatan) {
		this.idPenempatan = idPenempatan;
	}

	public String getKotaPenempatan() {
		return kotaPenempatan;
	}

	public void setKotaPenempatan(String kotaPenempatan) {
		this.kotaPenempatan = kotaPenempatan;
	}

	public BigDecimal getUmkPenempatan() {
		return umkPenempatan;
	}

	public void setUmkPenempatan(BigDecimal umkPenempatan) {
		this.umkPenempatan = umkPenempatan;
	}
}


